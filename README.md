Set a laminar CI to build your containers.

architecture
------------

```
                  ...................----------.
.-------.         .  .---------.    | build VM |
| gitea |---http---->| webhook |    '----------'
'-------'         .  '---------'               .
    .             .       |                    .
    .             .       v                    .
    .             .  .---------.               .
    '....git pull...>| laminar |.........      .
                  .  '---------'        v      .
                  .       |    ^   gpg verify  .
                  . docker|    '...git commits .
                  .  push |                    .
                  .       v                    .
                  . .----------.               .
                  . | registry |               .
                  . '----------'               .
                  .       .                    .
                  .  proxy.                    .
                  .       v                    .
                  .   .-------.                .
                  .   | nginx |                .
                  .   '-------'                .
                  ........^.....................
 .--------.               |
 | docker |---------------'
 '--------'  docker pull
```

webhook
-------

It will configure a webhook to listen in:
http://ip:9000/hooks/build\_image

The gitea of your organization should be configured to ping this url with {{ secret }} on each push event.

keys
----

The openpgp keys used to sign git commits are in the `keys` folder, ansible uses `keys.asc` to configure them in the server. We can generate this file by concatenating all the keys in one file:
```
$ cat keys/* > keys.asc
```
